# Notifier

###### Notification service

This is a notification service:

- Input: HTTP JSON request
- Output: Send email to user

In future, input will not change, but output can send notification via other channel (SMS, Instant Message,...)

### Installation

#### Download

Find binaries at [release](https://gitlab.com/chung1905/notifier/-/releases) page.

Move binary to `/usr/local/bin/notifier`

#### Config

1. Create `/home/notifier/.env` from `.env.sample`.

2. Change variables

#### Systemd service

Auto-start notifier with system

```
cp notifier.service /usr/lib/systemd/system/
cp .env /usr/local/etc/notifier.env
systemctl daemon-reload
systemctl enable notifier.service --now
```

### Usage

Curl:

```shell
curl --location --request POST '127.0.0.1:8080/email' \
     --header 'Content-Type: application/json' \
     --data-raw '{
"title": "Hello",
"body": "This is a test"
}'
```

PHP:

```php
<?php
$client = new http\Client;
$request = new http\Client\Request;
$request->setRequestUrl('127.0.0.1:8080/email');
$request->setRequestMethod('POST');
$body = new http\Message\Body;
$body->append('{
    "title": "Hello",
    "body": "This is a test"
}');
$request->setBody($body);
$request->setOptions(array());
$request->setHeaders(array(
  'Content-Type' => 'application/json'
));
$client->enqueue($request)->send();
$response = $client->getResponse();
echo $response->getBody();
```

Python:
```python
import requests

url = "127.0.0.1:8080/email"

payload="{\"title\":\"Hello\",\"body\":\"This is a test\"}"
headers = {
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=payload)

print(response.text)
```

### Build

#### Native compile

```
cargo build --release
```

#### Cross-compile on aarch64

1. Install [cross](https://github.com/rust-embedded/cross/)

2. Requires `openssl = { version = "0.10", features = ["vendored"] }` in `Cargo.toml`. We don't need this when native
   compile on `x86_64`. I didn't try native compile on `aarch64`.

3. Build command

```
cross build --release --target aarch64-unknown-linux-gnu
```
