use std::env;

use actix_web::{post, web, App, HttpResponse, HttpServer, Responder};
use lettre::transport::smtp::authentication::Credentials;
use lettre::{Message, SmtpTransport, Transport};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct NotificationWrapper {
    title: Option<String>,
    body: Option<String>,
}

struct Notification {
    title: String,
    body: String,
}

#[derive(Serialize)]
struct Response {
    status: bool,
    message: String,
}

impl Notification {
    fn unwrap_notification(wrapper: web::Json<NotificationWrapper>) -> Notification {
        let default: String = String::from("");
        Notification {
            title: wrapper.title.as_ref().unwrap_or(&default).clone(),
            body: wrapper.body.as_ref().unwrap_or(&default).clone(),
        }
    }
}

fn send_mail(noti: Notification) {
    let email = Message::builder()
        .from(env::var("SENDER_ADDRESS").unwrap().parse().unwrap())
        .to(env::var("RECEIVER_ADDRESS").unwrap().parse().unwrap())
        .subject(noti.title)
        .body(noti.body)
        .unwrap();

    let mailer = {
        let username = env::var("SMTP_USERNAME").unwrap();
        let password = env::var("SMTP_PASSWORD").unwrap();
        let server = env::var("SMTP_SERVER").unwrap();

        let creds = Credentials::new(username, password);

        SmtpTransport::relay(server.as_str())
            .unwrap()
            .credentials(creds)
            .build()
    };

    match mailer.send(&email) {
        Ok(_) => println!("Email sent successfully!"),
        Err(e) => panic!("Could not send email: {:?}", e),
    }
}

#[post("/email")]
async fn email_route(noti_json: web::Json<NotificationWrapper>) -> impl Responder {
    let noti = Notification::unwrap_notification(noti_json);
    send_mail(noti);
    HttpResponse::Ok().json(Response {
        status: true,
        message: String::from("OK"),
    })
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv::dotenv().ok();
    HttpServer::new(|| App::new().service(email_route))
        .bind("0.0.0.0:8080")?
        .run()
        .await
}
